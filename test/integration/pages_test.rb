require 'test_helper'

class PagesTest < ActionDispatch::IntegrationTest

  def setup
    @jim = users(:jim)
  end

  test "user show page shows user's posts (inside paragraphs) and not other posts" do
    get user_path(@jim)
    assert_select 'p', posts(:one).content #**
    assert_select 'p', posts(:two).content
    assert_select 'p', {count: 0, text: posts(:three).content}
  end

  test "home page shows featured posts (inside paragraphs) and not other posts" do
    featured_post = @jim.posts.create(content: "hello world!", featured: true)
    non_featured_post = @jim.posts.create(content: "not to be.", featured: false)
    get '/'
    assert_select 'p', featured_post.content
    assert_select 'p', {count: 0, text: non_featured_post.content}
  end

end