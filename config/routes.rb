Rails.application.routes.draw do
  get 'posts/show'

  get 'posts/index'

  root to: 'visitors#index'
  devise_for :users
  resources :users
  resources :posts
end
