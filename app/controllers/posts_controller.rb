class PostsController < ApplicationController
  def show
    @post = Post.find(params[:id])
  end

  def index
    @post = current_user.posts
  end
  
  def new
    @post = Post.new
  end
  
  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    if @post.save
      redirect_to @post
    else
      render "new"
    end
  end
  
  def edit
  	@post = Post.find(params[:id])
		if @post.user_id == current_user.id
			# ...
		else
			redirect_to root_path, :notice => "You dont have access to this post."
		end
  end

	def update
		@post = Post.find(params[:id])
		if @post.update(post_params)
			redirect_to @post
		else
			render "edit"
		end
	end

	def destroy
		if @post.user_id == current_user.id
			#go delete!
		else
			redirect_to root_path, :notice => "You dont have access to this post."
		end
		@post.destroy
		redirect_to posts_path
	end
	
	private

	def post_params
		params.require(:post).permit(:content, :featured_posts)
	end

	def find_post
		@post = Post.find(params[:id]) rescue nil
	end

end
